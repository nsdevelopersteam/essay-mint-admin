'use strict'

const hapi = require('hapi')
const Path = require('path')
const Handlebars = require('handlebars')
const HandlebarsRepeatHelper = require('handlebars-helper-repeat')
const CatboxRedis             = require('catbox-redis');

// extend handlebars instance
Handlebars.registerHelper('repeat', HandlebarsRepeatHelper)

// create new server instance and connection information
var server = new hapi.Server({
    cache: {
        engine: CatboxRedis,
        host: "localhost",
        port: 6379,
    },
    port: 7063,
    host: 'localhost',
});

/*var server = new hapi.Server({
    cache: {
        engine: CatboxRedis,
        host: "redis.mfportraits.com",
        port: 6379,
        password: '4334950390sasd9209432904was09d90asd0239490230d9sa90das90e90230492309ewasz09d09qe402309wsaadDDSDSDASD234234BGVJHNBUJLJHRDDAadfsd'
    },
});
*/

// register plugins, configure views and start the server instance
async function start () {
    // register plugins to server instance
    await server.register([
        {
            plugin: require('inert')
        },
        {
            plugin: require('vision')
        },
        {
            plugin: require('./web/authentication'),
        },
        {
            plugin: require('./web/base')
        },
        {
            plugin: require('./web/admin-authentication')
        },
        {
            plugin: require('./web/dashboard')
        }
    ])

    // view configuration
    const viewsPath = Path.resolve(__dirname, 'public', 'views')

    server.views({
        engines: {
            hbs: Handlebars
        },
        path: viewsPath,
        layoutPath: Path.resolve(viewsPath, 'layouts'),
        layout: 'authLayout',
        helpersPath: Path.resolve(viewsPath, 'helpers'),
        partialsPath: Path.resolve(viewsPath, 'partials'),
        context: {
            title: 'Worker Timer'
        }
    })

  // start your server
    try {
        await server.start()
        console.log(`Server started → ${server.info.uri}`)
    } catch (err) {
        console.error(err)
        process.exit(1)
    }
}

start()