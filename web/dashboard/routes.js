'use strict'

const Handler = require('./handler')
const Joi = require("joi")

const Routes = [
    {
        method: 'GET',
        path: '/panel',
        config: {
          auth: {
              mode: 'required',
              strategy: 'session',
          },
        },
        handler: Handler.panel.handler
    },

    {
        method: 'GET',
        path: '/logout',
        config: {
          auth: {
              mode: 'required',
              strategy: 'session',
          },
        },
        handler: Handler.logout.handler
    },

    {
        method: 'GET',
        path: '/orders/getall',
        config: {
            auth: {
                mode: 'required',
                strategy: 'session'
            },
            handler: Handler.getOrders.handler
        }    
    },

    {
        method: 'GET',
        path: '/orders/detail/{publicId}',
        config: {
            auth: {
                mode: 'required',
                strategy: 'session'
            },
            handler: Handler.getOrder.handler
        }    
    },

    {
        method: 'PATCH',
        path: '/order/changestatus',
        config: {
            auth: {
                mode: 'required',
                strategy: 'session',
            },
            validate: {
                payload: {
                    publicId : Joi.string().required(),
                    newStatus: Joi.string().required(),
                }
            },
            handler: Handler.ChangeOrderStatus.handler
        }
    },
]

module.exports = Routes