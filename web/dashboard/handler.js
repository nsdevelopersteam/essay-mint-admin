'use strict'

const APIRequests = require("../../communication/APIRequests")
const currencyFormatter = require('currency-formatter');

const Handler = {
    panel: {
        plugins: {
            'hapi-auth-cookie': {
                redirectTo: true
            }
        },
        handler: async function test (request, h) {

            const promise = new Promise((resolve, reject) => {
                APIRequests.GetStatistics(request.auth.credentials)
                .then(systemStats => {
                    var data = {
                        nOfOrders : systemStats.stats.totalNumberOfOrders,
                        nOfVisitors : systemStats.stats.numberOfVisits,
                        undeposit: currencyFormatter.format(systemStats.stats.undepositedAmount, { code: 'USD' }),
                        total: currencyFormatter.format(systemStats.stats.totalEarned, { code: 'USD' })
                    }
                    resolve(h.view('dashboard', data , { layout: 'mainLayout' }))
                })
                .catch(error => {
                    reject(h.view('500').code(500))
                })
            })
            return promise
        }
    },

    getOrders: {
        plugins: {
            'hapi-auth-cookie': {
                redirectTo: false
            }
        },
        handler: async function getOrders(request, h) {
            const promise = new Promise((resolve, reject) => {
                APIRequests.GetOrders(request.auth.credentials)
                .then(allOrdersResponse => {
                    resolve({data: allOrdersResponse.records})
                })
                .catch(error => {
                    reject(error)
                })
            })

            return promise
        }
    },

    getOrder: {
        plugins: {
            'hapi-auth-cookie': {
                redirectTo: false
            }
        },
        handler: async function getOrder(request, h) {
            const promise = new Promise((resolve, reject) => {
                APIRequests.GetOrderDetails(request.auth.credentials, request.params.publicId)
                .then(responseOrder => {
                    var responseOrderDetails = {
                        statusCode: 200,
                        error: null,
                        message: 'success',
                        details: responseOrder.order
                    }
                    resolve(responseOrderDetails)
                })
                .catch(error => {
                    reject(error)
                })
            })

            return promise
        }
    },

    logout: {
        plugins: {
            'hapi-auth-cookie': {
                redirectTo: false
            }
        },

        handler: async function logout(request, h) {
            request.cookieAuth.clear();
            return h.redirect('/panel');
        }
    },

    ChangeOrderStatus: {
        plugins: {
            'hapi-auth-cookie': {
                redirectTo: false
            }
        },
        handler: async function ChangeOrderStatus(request, h) {
            const promise = new Promise((resolve, reject) => {
                APIRequests.ChangeOrderStatus(request.auth.credentials, request.payload.publicId, request.payload.newStatus)
                .then(successResponse => {
                    var successResponseJson = {
                        statusCode: 200,
                        error: null,
                        message: 'success',
                        redirect: '/panel'
                    }
                    resolve(successResponseJson)
                })
                .catch(error => {
                    reject(error)
                })
            })

            return promise
        }
    },
}

module.exports = Handler