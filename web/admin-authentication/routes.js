'use strict'

const Handler = require('./handler')
const joi = require('joi');

const Routes = [
    {
      method: 'GET',
      path: '/login',
      config: Handler.login
    },
    {
      method: 'POST',
      path: '/auth',
      config: {
          auth: {
              mode: 'try',
              strategy: 'session',
          },
          validate: {
              payload: {
                  email : joi.string().email().required(),
                  password: joi.string().required(),
              }
          },
          handler: Handler.loginAction.handler
      }
  },
]

module.exports = Routes