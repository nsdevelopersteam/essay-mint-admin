'use strict'

const APIRequest = require("../../communication/APIRequests");
const Crypto = require('crypto');
const boom = require('boom');

const Handler = {
    login: {
        plugins: {
            'hapi-auth-cookie': {
                redirectTo: false
            }
        },
        handler: async function test (request, h) {
            return h.view(
                'login',
                {
                },
                { layout: 'authLayout' }
            )
        }
    },

    loginAction: {
        plugins: {
            'hapi-auth-cookie': {
                redirectTo: false
            }
        },
        handler: async (request, h) => {
            const promise = new Promise((resolve, reject) => {
                         
                APIRequest.SignInAdmin(request.payload.email, request.payload.password)
                .then(async LoggedInfo => {
                    var sid = Crypto.randomBytes(32).toString('hex');
                
                    let account = LoggedInfo.authToken
                    
                    await request.server.app.cache.set(sid, {account}, 0)
                    request.cookieAuth.set({ sid });
                    
                    resolve({
                        statusCode: 200,
                        error : null,
                        message: 'redirect',
                        redirect: '/panel'
                    })
                })
                .catch(error => {
                    if(error.message === "change password") {
                        resolve({
                            statusCode: 200,
                            error : null,
                            message: 'redirect',
                            redirect: '/panel'
                        })
                    }
                    else {
                        reject(boom.badRequest(error.message))
                    }
                })
            })
            return promise
        }
    }
}

module.exports = Handler