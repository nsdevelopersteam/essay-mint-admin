'use strict'

const Boom = require('boom')

const Handler = {
    index: {
        plugins: {
            'hapi-auth-cookie': {
                redirectTo: false
            }
        },
        
        handler: async function test (request, h) {
    
            return h.view(
                'login', {}, { layout: 'authLayout' }
            )
        }
    },

    css: {
        plugins: {
            'hapi-auth-cookie': {
                redirectTo: false
            }
        },
        handler: {
            directory: { path: './public/css' }
        }
    },

    js: {
        plugins: {
            'hapi-auth-cookie': {
                redirectTo: false
            }
        },
        handler: {
            directory: { path: './public/js' }
        }
    },

    images: {
        plugins: {
            'hapi-auth-cookie': {
                redirectTo: false
            }
        },
        handler: {
            directory: { path: './public/images' }
        }
    },

    fonts: {
        plugins: {
            'hapi-auth-cookie': {
                redirectTo: false
            }
        },
        handler: {
            directory: { path: './public/fonts' }
        }
    },

    missing: {
        handler: (request, h) => {
            const accept = request.headers.accept

            if (accept && accept.match(/json/)) {
                return Boom.notFound('Its not your fault, this resource isn’t available.')
            }

            return h.view('404').code(404)
        }
    }

}

module.exports = Handler