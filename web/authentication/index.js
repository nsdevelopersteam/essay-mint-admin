'use strict'

async function register (server, options) {
    // declare dependencies to hapi-auth-* plugins

    await server.register(require('hapi-auth-cookie'));
 
    const cache = server.cache({ segment: 'sessions', expiresIn: 3 * 24 * 60 * 60 * 1000 });
    server.app.cache = cache;

    server.auth.strategy('session', 'cookie', {
        password: 'password-should-be-32-characters',
        cookie: 'sid-example',
        redirectTo: '/login',
        isSecure: false,
        validateFunc: async (request, session) => {

            const cached = await cache.get(session.sid);
            const out = {
                valid: !!cached
            };

            if (out.valid) {
                out.credentials = cached.account;
            }

            return out;
        }
    });
}

exports.plugin = {
    name: 'authentication',
    version: '1.0.0',
    register
}