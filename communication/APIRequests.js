const configuration               = require('../config');
const Client                      = require('node-rest-client').Client;
const Q                           = require('q');

var options = {
    connection: {
        rejectUnauthorized: false,
    },
    requestConfig: {
        timeout: 60000,
        noDelay: true,
        keepAlive: true,
        keepAliveDelay: 1000
    },
    responseConfig: {
        timeout: 300000
    }
};

var client = new Client(options);

module.exports = {
    SignInAdmin: function(emailParam, passwordParam) {
        var deferred = Q.defer();
        var args = {
            headers: { 
                "Content-Type": "application/json",
            },
            data: {
                email: emailParam,
                password: passwordParam
            }
        };
        client.post(configuration.API.developmentURL + "/admin/auth", args, function (dataBasicInfo, response) {
            if(dataBasicInfo['statusCode'] == 200) {
                deferred.resolve(dataBasicInfo);
            }
            else {
                deferred.reject(dataBasicInfo);
            }
        })
        
        return deferred.promise;
    },

    GetStatistics: function(authTokenParam) {
        var deferred = Q.defer();
        var args = {
            headers: { 
                "Authorization" : 'Bearer ' + authTokenParam,
                "Content-Type": "application/json",
            },
        };
        client.get(configuration.API.developmentURL + "/admin/getstats", args, function (dataBasicInfo, response) {
            if(dataBasicInfo['statusCode'] == 200) {
                deferred.resolve(dataBasicInfo);
            }
            else {
                deferred.reject(dataBasicInfo);
            }
        })
        
        return deferred.promise;
    },

    GetOrders: function(authTokenParam) {
        var deferred = Q.defer();
        var args = {
            headers: { 
                "Authorization" : 'Bearer ' + authTokenParam,
                "Content-Type": "application/json",
            },
        };
        client.get(configuration.API.developmentURL + "/order/getall", args, function (dataBasicInfo, response) {
            if(dataBasicInfo['statusCode'] == 200) {
                deferred.resolve(dataBasicInfo);
            }
            else {
                deferred.reject(dataBasicInfo);
            }
        })
        
        return deferred.promise;
    },

    GetOrderDetails: function(authTokenParam, publicIdParam) {
        var deferred = Q.defer();
        var args = {
            headers: { 
                "Authorization" : 'Bearer ' + authTokenParam,
                "Content-Type": "application/json",
            },
        };
        client.get(configuration.API.developmentURL + "/order/details/" + publicIdParam, args, function (dataBasicInfo, response) {
            if(dataBasicInfo['statusCode'] == 200) {
                deferred.resolve(dataBasicInfo);
            }
            else {
                deferred.reject(dataBasicInfo);
            }
        })
        
        return deferred.promise;
    },

    ChangeOrderStatus: function(authTokenParam, publicIdParam, newStatusParam) {
        var deferred = Q.defer();
        var args = {
            headers: { 
                "Authorization" : 'Bearer ' + authTokenParam,
                "Content-Type": "application/json",
            },
            data: {
                "publicId": publicIdParam,
                "newStatus": newStatusParam
            }
        };
        client.patch(configuration.API.developmentURL + "/order/changestatus", args, function (dataBasicInfo, response) {
            if(dataBasicInfo['statusCode'] == 200) {
                deferred.resolve(dataBasicInfo);
            }
            else {
                deferred.reject(dataBasicInfo);
            }
        })
        
        return deferred.promise;
    },
}