$(document).ready(function () {

    function CheckName(nameParam) {
        if(nameParam.toString().length === 0) {
            return false
        }

        return true
    }

    function CheckEmail(emailParam) {
        return /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(emailParam)    
    }

    function CheckPassword(passwordParam) {
        if(passwordParam.toString().length === 0) {
            return false
        }

        return true
    }

    $('#frmLogin').on("submit", function (e) {     

        blockView = () => {
            $("#btnLogin").prop("disabled", true); 
            $("#txtEmail").prop("disabled", true); 
            $("#txtPassword").prop("disabled", true); 
        }

        releaseView = () => {
            $("#btnLogin").prop("disabled", false);
            $("#txtEmail").prop("disabled", false); 
            $("#txtPassword").prop("disabled", false); 
        }

        e.preventDefault();
        blockView();

        if(!CheckEmail($("#txtEmail").val())) {
            releaseView();
            swal("Error", "Please insert a valid email", 'warning');
        }
        else if(!CheckPassword($("#txtPassword").val())) {
            releaseView();
            swal("Error", "Please insert a valid password", 'warning');
        }
        else {
            try {
                $.ajax({
                    type: "POST",
                    url: "auth",
                    timeout: 60000,
                    async: true,
                    data: { email: $("#txtEmail").val(), password: $("#txtPassword").val()},
                    success: function(response) { 
                        window.location.href = response['redirect'];
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        releaseView();
                        if(errorThrown === "Bad Request") {
                            swal("Error", "Please insert your information correctly", 'warning');
                        }   
                        else {
                            swal("Error", "Something is broken please try again later", 'warning');
                        }
                        
                    }
                });   
            } catch (error) {
                releaseView();
                swal("Error", "Something is broken please try again later", 'warning');
            }
        }
    }); 
});