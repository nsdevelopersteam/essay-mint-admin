FROM node

WORKDIR /app
COPY . .

EXPOSE 7063
 
CMD ["npm", "install"]
CMD ["node", "server.js"]